package org.glassfish.jersey.client.async;

import io.vavr.Function0;
import io.vavr.Function1;
import io.vavr.Function2;
import io.vavr.Function3;

import javax.ws.rs.client.WebTarget;
import java.util.concurrent.CompletableFuture;

/**
 * A convenience entry point that allows to provide {@link WebTarget} at the beginning of initialization.
 * It is designed to use with a typical IoC container, when base url is known during initialization.
 * All initX() methods share the following characteristics:
 * 
 * <p> 1. Take a method reference as an argument.
 * <p> 2. Are computationally expensive to call and should not called when performing each remote invocation.
 * <p> 3. Produce a 'caller' function that will trigger the remote call when invoked.
 * <p> 4. The returned function is immutable and safe to share between threads.
 * <p> 5. Function's argument types will match the argument types of the provided method, except that primitive types 
 * will be converted to their object counterpart, e.g. int -> Integer.
 * 
 * @param <T> Resource type
 */
public class AsyncWebResource<T> {
    private final AsyncWebResourceFactory<T> factory;
    /** Base url to call */
    private final WebTarget target;
    
    public static <T> AsyncWebResource<T> of(WebTarget target, Class<T> resInterface) {
        return new AsyncWebResource<>(target, AsyncWebResourceFactory.of(resInterface));
    }

    private AsyncWebResource(WebTarget target, AsyncWebResourceFactory<T> factory) {
        this.target = target;
        this.factory = factory;
    }

    public <R> Function0<CompletableFuture<R>> init0(Function1<T, R> method) {
        // This looks simpler that other overloaded methods, because when method has no arguments,
        // there is no need to scan for the argument types.
        return factory.init0(method).apply(target);
    }

    public <V1, R> Function1<V1, CompletableFuture<R>> init1(Function2<T, V1, R> method) {
        return factory.init1(method).apply(target);
    }

    public <V1, V2, R> Function2<V1, V2, CompletableFuture<R>> init2(Function3<T, V1, V2, R> method) {
        return factory.init2(method).apply(target);
    }
}
