package org.glassfish.jersey.client.async;

import io.vavr.API;
import io.vavr.Function0;
import io.vavr.Function1;
import io.vavr.Function2;
import io.vavr.Function3;
import io.vavr.Tuple2;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import io.vavr.collection.Stream;
import io.vavr.control.Option;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.AsyncInvoker;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static io.vavr.API.Case;
import static io.vavr.API.Match;
import static io.vavr.API.Tuple;
import static org.glassfish.jersey.client.async.MethodCaptor.def;

/**
 * A primary, low-level entry point that allows to perform computationally-heavy initialization once during the
 * initialization phase, even if {@link WebTarget} is not yet available.
 * <p>
 * All initX() methods share the following characteristics:
 * <p> 1. Take a method reference as an argument.
 * <p> 2. Are computationally expensive to call as reflection is being used to iterate over the methods.
 * <p> 3. Produce a function that takes a {@link WebTarget} and returns a new 'caller' function that will call the
 * remote method when invoked.
 * <p> 4. Both the original returned object and 'caller' function are immutable and safe to share between threads.
 * <p> 5. Function's argument types will match the argument types of the provided method, except that primitive types
 * will be converted to their object counterpart, e.g. int -> Integer.
 * <p>
 * Note that if {@link WebTarget} is available during initialization, it more convenient to use 
 * {@link AsyncWebResource}, as there will be no need to provice target to every method. 
 *
 * @param <T> Resource interface type
 */
public class AsyncWebResourceFactory<T> {
    private static final String[] EMPTY = {};
    private static final MultivaluedMap<String, Object> EMPTY_HEADERS = new MultivaluedHashMap<>();
    private static final Form EMPTY_FORM = new Form();

    /** List of annotation classes that mark parameter annotations */
    private static final Set<Class<? extends Annotation>> PARAM_ANNOTATION_CLASSES = HashSet.of(
        PathParam.class, QueryParam.class, HeaderParam.class, CookieParam.class, MatrixParam.class, FormParam.class
    );
    private static final Predicate<Annotation> IS_PARAM_ANNOTATION = ann ->
        PARAM_ANNOTATION_CLASSES.exists(clazz -> clazz.isAssignableFrom(ann.getClass()));

    /** Resource interface to harness JAX-RS annotation metadata */
    private final Class<T> resInterface;
    /** Default headers, explicitly specified at instantiation */
    private final MultivaluedMap<String, Object> headers;
    /** Default cookies, explicitly specified at instantiation */
    private final List<Cookie> cookies;

    /**
     * Creates an instance of the {@link AsyncWebResource}, that can produce functions asynchronously
     * calling resource methods of a given class.
     *
     * @param resInterface resource interface.
     * @param <T> Resource interface type
     * @return A new instace of the factory. The returned object is immutable and can be safely shared between threads.
     */
    public static <T> AsyncWebResourceFactory<T> of(Class<T> resInterface) {
        return new AsyncWebResourceFactory<>(resInterface, EMPTY_HEADERS, Collections.emptyList());
    }

    private AsyncWebResourceFactory(Class<T> resInterface, MultivaluedMap<String, Object> headers, List<Cookie> cookies) {
        this.resInterface = resInterface;
        this.headers = new MultivaluedHashMap<String, Object>(headers);
        this.cookies = new ArrayList<>(cookies);
    }

    public <R> Function1<WebTarget, Function0<CompletableFuture<R>>> init0(Function1<T, R> method) {
        // This looks simpler that other overloaded methods, because when method has no arguments,
        // there is no need to scan for the argument types.
        return this.<R>init(method::apply).andThen(invoker -> () -> invoker.apply(new Object[]{}));
    }

    public <V1, R> Function1<WebTarget, Function1<V1, CompletableFuture<R>>> init1(Function2<T, V1, R> method) {
        return this.<R>init(o ->
            scanCall(1, types -> method.apply(o).apply(def(types[0])))
        ).andThen(invoker ->
            (v1) -> invoker.apply(new Object[]{v1})
        );
    }

    public <V1, V2, R> Function1<WebTarget, Function2<V1, V2, CompletableFuture<R>>> init2(Function3<T, V1, V2, R> method) {
        return this.<R>init(o ->
            scanCall(2, types -> method.apply(o).apply(def(types[0]), def(types[1])))
        ).andThen(invoker ->
            (v1, v2) -> invoker.apply(new Object[]{v1, v2})
        );
    }

    private <R> Function1<WebTarget, Function1<Object[], CompletableFuture<R>>> init(Consumer<T> methodCaller) {
        Method method = MethodCaptor.capture(resInterface, methodCaller);
        String contentType = getContentType(resInterface, method);
        String[] accepts = getAcceptedMediaType(resInterface, method);
        String methodName = getHttpMethodName(method);

        GenericType<R> responseType = new GenericType<>(method.getGenericReturnType());
        Annotation[][] paramAnns = method.getParameterAnnotations();
        ArgumentMatcher matcher = new ArgumentMatcher(Stream.of(paramAnns).zipWithIndex());

        // Calculates a new WebTarget from a base and method arguments.
        // Path and query parameters can affect this calculation.
        Option<Function1<Object[], Function1<WebTarget, WebTarget>>> fnTarget = matcher.of(
            instanceOf(PathParam.class, pp -> pathParam(pp.value())),
            instanceOf(QueryParam.class, qp -> queryParam(qp.value()))
        ).reduceOption(chain());

        // Calculates headers from method arguments
        Function1<Object[], MultivaluedHashMap<String, Object>> fnHeaders = rotate(matcher.of(
            instanceOf(HeaderParam.class, hp -> headerParam(hp.value()))
        )).andThen(AsyncWebResourceFactory::toMultivaluedMap);

        // Calculates cookies from method arguments
        Function1<Object[], Stream<Cookie>> fnCookies = args -> matcher.of(
            instanceOf(CookieParam.class, cp -> cookieParam(cp.value()))
        ).flatMap(fn -> fn.apply(args));

        // Entity is the first argument without any parameter annotation
        Option<Function1<Object[], Entity<Object>>> fnEntity = entityIndex(paramAnns).map(index ->
            entity(contentType, index, method.getGenericParameterTypes()[index])
        );

        return addPathFrom(resInterface).andThen(addPathFrom(method)).andThen(baseTarget -> args -> {
            // TODO Forms, MatrixParam and CookieParam are currently not supported

            WebTarget target = fnTarget.map(fn -> fn.apply(args).apply(baseTarget)).getOrElse(baseTarget);

            MultivaluedHashMap<String, Object> headers = new MultivaluedHashMap<String, Object>(this.headers);
            headers.putAll(fnHeaders.apply(args));
            Stream<Cookie> cookies = fnCookies.apply(args);

            // The order is important! Resets all headers first, then if @Produces is defined,
            // propagate values into Accept header; empty array is NO-OP
            Invocation.Builder builder = target.request().headers(headers).accept(accepts);
            // Apply default cookie values first
            this.cookies.forEach(builder::cookie);
            cookies.forEach(builder::cookie);

            AsyncInvoker invoker = builder.async();
            return fnEntity.map(fn ->
                (CompletableFuture<R>) invoker.method(methodName, fn.apply(args), responseType)
            ).getOrElse(() ->
                (CompletableFuture<R>) invoker.method(methodName, responseType)
            );
        });

    }

    private static Function1<Object[], Entity<Object>> entity(String contentType, Integer index, Type type) {
        return args -> {
            Object entity = (type instanceof ParameterizedType) ? new GenericEntity<>(args[index], type) : args[index];
            return Entity.entity(entity, contentType);
        };
    }

    private static Option<Integer> entityIndex(Annotation[][] paramAnns) {
        return Stream.of(paramAnns).indexWhereOption(annotations ->
            !Stream.of(annotations).exists(IS_PARAM_ANNOTATION)
        );
    }

    private static <T> String[] getAcceptedMediaType(Class<T> resInterface, Method method) {
        Produces produces = Optional.ofNullable(method.getAnnotation(Produces.class)).orElseGet(
            () -> resInterface.getAnnotation(Produces.class)
        );
        return Optional.ofNullable(produces).map(Produces::value).orElse(EMPTY);
    }

    private static Function1<Object, Stream<Cookie>> cookieParam(String name) {
        return value -> {
            Stream<Object> os = (value instanceof Collection) ? Stream.ofAll((Collection<?>) value) : Stream.of(value);
            return os.map(v -> {
                if ((v instanceof Cookie)) {
                    Cookie c = (Cookie) v;
                    return c.getName() == null ? new Cookie(name, c.getValue(), c.getPath(), c.getDomain(), c.getVersion()) : c;
                } else {
                    return new Cookie(name, v.toString());
                }
            });
        };
    }

    private static Function1<Object, Function1<WebTarget, WebTarget>> queryParam(String name) {
        return value -> t -> t.queryParam(name, converge(value));
    }

    private static Function1<Object, Function1<WebTarget, WebTarget>> pathParam(String name) {
        return value -> t -> t.resolveTemplate(name, value);
    }

    private static Function1<Object, Tuple2<String, Object[]>> headerParam(String name) {
        return value -> Tuple(name, converge(value));
    }

    private static String getContentType(Class<?> ifc, Method method) {
        return Stream.of(
            method.getAnnotation(Consumes.class), ifc.getAnnotation(Consumes.class)
        ).filter(Objects::nonNull).flatMap(types ->
            Stream.of(types.value())
        ).headOption().getOrElseThrow(() ->
            new IllegalArgumentException("Unable to identify content type from " + method.toString() + " and " + ifc.getName())
        );
    }

    private static String getHttpMethodName(Method method) {
        Stream<AnnotatedElement> elements = Stream.<AnnotatedElement>of(method).appendAll(
            Stream.of(method.getAnnotations()).map(Annotation::annotationType)
        );
        return elements.map(
            ae -> ae.getAnnotation(HttpMethod.class)
        ).filter(Objects::nonNull).map(HttpMethod::value).headOption().getOrElseThrow(() ->
            // Might be subresource locator, however we don't support them, yet
            new UnsupportedOperationException("Unknown HTTP method")
        );
    }


    private static MultivaluedHashMap<String, Object> toMultivaluedMap(Stream<Tuple2<String, Object[]>> entries) {
        MultivaluedHashMap<String, Object> map = new MultivaluedHashMap<>();
        entries.forEach(entry -> map.addAll(entry._1, entry._2));
        return map;
    }

    private static Function1<WebTarget, WebTarget> addPathFrom(AnnotatedElement ae) {
        return t -> Optional.ofNullable(ae.getAnnotation(Path.class)).map(p -> t.path(p.value())).orElse(t);
    }

    private static <T> Function1<Object[], T> byIndex(Integer index, Function1<Object, T> f) {
        return f.compose(args -> args[index]);
    }

    /**
     * When primitives, such as 'int' or 'long' are present in the method argument declaration,
     * a dummy null values will trigger NPE due to autounboxing.
     * This method iterates all the methods in class and uses their signatures attempting to a valid
     * stub for arguments array.
     *
     * @param arity number of method arguments
     * @param call code fragment that will perform the invokation, given the method parameter types.
     */
    private void scanCall(int arity, Consumer<Class<?>[]> call) {
        // Happens when primitives, such as 'int' or 'long' are in the original method argument declaration.
        Stream<Class<?>[]> argTypes = Stream.of(resInterface.getMethods()).map(Method::getParameterTypes).filter(
            types -> types.length == arity
        );
        for (Class<?>[] types : argTypes) {
            try {
                call.accept(types);
                // We got a hit, break out of the loop so captor can complete its job
                break;
            } catch (NullPointerException | ClassCastException ex) {
                // Not lucky, proceed iterating in the loop
            }
        }
    }

    /**
     * Converts value to array of {@link Object}.
     * If the value is a collection, convert it to array. Otherwise, wrap it into array explicitly.
     * This is required to handle method call that take varargs, to prevent compiler from double-wrapping it.
     *
     * @param value value to converge
     * @return if the value is collection, array of items. Otherwise, array containing the value.
     */
    private static Object[] converge(Object value) {
        return value instanceof Collection ? ((Collection) value).toArray() : new Object[]{value};
    }

    @SuppressWarnings("unchecked")
    private static <T, C extends T, V> Match.Case<T, V> instanceOf(Class<C> clazz, Function<C, V> transformer) {
        return Case(API.$(clazz::isInstance), ext -> transformer.apply((C) ext));
    }

    /**
     * A binary operator to merge two functions, executing them in chain
     */
    private static <T, V> BinaryOperator<Function1<T, Function1<V, V>>> chain() {
        return (a, b) -> o -> a.apply(o).andThen(b.apply(o));
    }

    /**
     * Converts a stream of functions that take same type of argument
     * into a single function that produces stream of results.
     *
     * @param functions stream of functions to convert
     * @param <T> argument type
     * @param <V> value type
     */
    private static <T, V> Function1<T, Stream<V>> rotate(Stream<Function1<T, V>> functions) {
        return argument -> functions.map(fn -> fn.apply(argument));
    }


    /**
     * Instance utility to provide concise semantics for matching argument annotations.
     */
    private static class ArgumentMatcher {
        private Stream<Tuple2<Annotation[], Integer>> arguments;

        ArgumentMatcher(Stream<Tuple2<Annotation[], Integer>> arguments) {
            this.arguments = arguments;
        }

        @SafeVarargs
        final <V> Stream<Function1<Object[], V>> of(Match.Case<? extends Annotation, Function1<Object, V>>... cases) {
            return arguments.flatMap(args ->
                args.apply((anns, index) ->
                    Stream.of(anns).flatMap(a -> Match(a).option(cases)).headOption().map(f -> byIndex(index, f))
                )
            );
        }
    }
}
