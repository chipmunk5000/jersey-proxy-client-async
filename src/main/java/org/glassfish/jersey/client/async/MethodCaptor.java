package org.glassfish.jersey.client.async;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.function.Consumer;

import static io.vavr.API.Option;

/**
 * Utility class to provide a reasonable degree of compile-time checking of method references.
 */
class MethodCaptor {

    /**
     * Captures method call and produces a {@code java.lang.reflect.Method} object that represent it.
     * This is quite expensive operation that relies on reflection, so it is recommended to have it called once,
     * as part of some initialization.
     *
     * @param resourceInterface Interface to capture method from
     * @param method A function that triggers a single method call. Note that method does not have to take only one
     * argument, there can be any number of them, just use null's or whatever dummy values, they will be ignored anyway.
     * @param <T> Resource type
     * @return a java Method object representing the method
     */
    static <T> Method capture(final Class<T> resourceInterface, Consumer<T> method) {
        CapturingHandler<T> captor = new CapturingHandler<>(resourceInterface);
        ClassLoader classLoader = AccessController.doPrivileged(
            (PrivilegedAction<ClassLoader>) resourceInterface::getClassLoader
        );
        T subj = (T) Proxy.newProxyInstance(classLoader, new Class[]{resourceInterface}, captor);
        method.accept(subj);

        return Option(captor.method).getOrElseThrow(() ->
            new IllegalArgumentException("No method captured on [" + resourceInterface.getName() + "]")
        );
    }

    public static class CapturingHandler<T> implements InvocationHandler {
        private Class<T> subject;
        private Method method;

        CapturingHandler(Class<T> subject) {
            this.subject = subject;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Exception {
            Class<?> declaringClass = method.getDeclaringClass();
            if (Object.class.equals(declaringClass)) {
                // Short-circuit methods like hashCode() and equals() to prevent debugger from messing with capture
                return method.invoke(this, args);
            } else {
                if (subject.equals(declaringClass)) {
                    if (this.method == null) {
                        this.method = method;
                    } else {
                        throw new IllegalArgumentException(
                            "Multiple method calls found: [" + method.toString() + "] and [" + this.method.toString() + "]"
                        );
                    }
                }
                return def(method.getReturnType());
            }
        }
    }

    /**
     * If return value is primitive, using null as a dummy value will cause NPE on autounboxing.
     * This provides a default value to work around that problem.
     */
    @SuppressWarnings("unchecked")
    static <T> T def(Class<?> type) {
        // The trick with creating new array is not very idiomatic, but at least is concise
        return type.isPrimitive() ? (T) Array.get(Array.newInstance(type, 1), 0) : null;
    }

}