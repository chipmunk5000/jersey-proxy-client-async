package org.glassfish.jersey.client.async;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AsyncWebResourceTest<T> {
    private final JerseyTest delegate;
    private final Class<T> clazz;
    private final Class<? extends T> implClass;

    protected AsyncWebResource<T> resource;

    protected AsyncWebResourceTest(Class<T> clazz, Class<? extends T> implClass) {
        this.clazz = clazz;
        this.implClass = implClass;
        delegate = new JerseyTest() {
            protected ResourceConfig configure() {
                // mvn test -Djersey.config.test.container.factory=org.glassfish.jersey.test.inmemory.InMemoryTestContainerFactory
                // mvn test -Djersey.config.test.container.factory=org.glassfish.jersey.test.grizzly.GrizzlyTestContainerFactory
                // mvn test -Djersey.config.test.container.factory=org.glassfish.jersey.test.jdkhttp.JdkHttpServerTestContainerFactory
                // mvn test -Djersey.config.test.container.factory=org.glassfish.jersey.test.simple.SimpleTestContainerFactory
                enable(TestProperties.LOG_TRAFFIC);
                // enable(TestProperties.DUMP_ENTITY);
                return new ResourceConfig(AsyncWebResourceTest.this.implClass);
            }
        };
    }

    @BeforeAll
    void init() throws Exception {
        delegate.setUp();
        resource = AsyncWebResource.of(delegate.target(), clazz);
    }

    @AfterAll
    void dispose() throws Exception {
        delegate.tearDown();
    }

}
