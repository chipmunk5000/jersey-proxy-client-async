package org.glassfish.jersey.client.async;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

@Path("duck")
@Consumes({MediaType.APPLICATION_JSON})
public interface JsonDuckApi {
    @GET
    @Path("get")
    @Produces(MediaType.TEXT_PLAIN)
    String getPlain();

    @GET
    @Path("get/quack/1/{count}")
    @Produces(MediaType.TEXT_PLAIN)
    String quack(@PathParam("count") int count);

    @GET
    @Path("header")
    @Produces(MediaType.TEXT_PLAIN)
    String getByNameHeader(@HeaderParam("header-name") String name);

    @GET
    @Path("header-list")
    @Produces(MediaType.TEXT_PLAIN)
    String getByNameHeaderList(@HeaderParam("header-items-list") List<String> items);

    @GET
    @Path("header-set")
    @Produces(MediaType.TEXT_PLAIN)
    String getByNameHeaderSet(@HeaderParam("header-items-set") Set<String> items);

    @GET
    @Path("header-sortedset")
    @Produces(MediaType.TEXT_PLAIN)
    String getByNameHeaderSortedSet(@HeaderParam("header-items-sorted") SortedSet<String> items);

    @GET
    @Path("cookie")
    @Produces(MediaType.TEXT_PLAIN)
    String getByNameCookie(@CookieParam("cookie-name") String name);

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("matrix")
    String getByNameMatrix(@MatrixParam("matrix-name") String name);

    @POST
    @Path("post/quack/2/{name}")
    String quack(@PathParam("name") String name, @QueryParam("count") int count);
}
