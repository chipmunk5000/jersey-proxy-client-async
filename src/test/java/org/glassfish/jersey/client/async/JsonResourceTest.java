package org.glassfish.jersey.client.async;

import io.vavr.Function0;
import io.vavr.Function1;
import io.vavr.Function2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

class JsonResourceTest extends AsyncWebResourceTest<JsonDuckApi> {
    JsonResourceTest() {
        super(JsonDuckApi.class, JsonDuckResource.class);
    }

    @Test
    void getNoArgsReturnsString() throws Exception {
        Function0<CompletableFuture<String>> subj = resource.init0(JsonDuckApi::getPlain);
        String result = subj.apply().get();
        Assertions.assertEquals("quack quack", result);
    }

    @Test
    void getIntArgReturnsString() throws Exception {
        Function1<Integer, CompletableFuture<String>> subj = resource.init1(JsonDuckApi::quack);
        String result = subj.apply(3).get();
        Assertions.assertEquals("quack quack quack", result);
    }

    @Test
    void postStringIntReturnsString() throws Exception {
        Function2<String, Integer, CompletableFuture<String>> subj = resource.init2(JsonDuckApi::quack);
        String result = subj.apply("Gasoline", 3).get();
        Assertions.assertEquals("Duck 'Gasoline' quacks 3 times", result);
    }

    @Test
    void getByNameCookie() throws ExecutionException, InterruptedException {
        Function1<String, CompletableFuture<String>> subj = resource.init1(JsonDuckApi::getByNameCookie);
        String result = subj.apply("Launchpad").get();
        Assertions.assertEquals("cookie Launchpad", result);
    }

    @Test
    void getHeaderParam() throws ExecutionException, InterruptedException {
        Function1<String, CompletableFuture<String>> subj = resource.init1(JsonDuckApi::getByNameHeader);
        String result = subj.apply("Launchpad").get();
        Assertions.assertEquals("Duck 'Launchpad' is here", result);
    }

    @Test
    void getHeaderListParam() throws ExecutionException, InterruptedException {
        Function1<List<String>, CompletableFuture<String>> subj = resource.init1(JsonDuckApi::getByNameHeaderList);
        String result = subj.apply(Arrays.asList("Launchpad", "Gasoline")).get();
        Assertions.assertEquals("List: Launchpad,Gasoline", result);
    }

    @Test
    void getHeaderSetParam() throws ExecutionException, InterruptedException {
        Function1<Set<String>, CompletableFuture<String>> subj = resource.init1(JsonDuckApi::getByNameHeaderSet);
        String result = subj.apply(new HashSet<>(Arrays.asList("Launchpad", "Gasoline"))).get();
        Assertions.assertEquals("Set: Launchpad,Gasoline", result);
    }

    @Test
    void getHeaderSortedSetParam() throws ExecutionException, InterruptedException {
        Function1<SortedSet<String>, CompletableFuture<String>> subj = resource.init1(JsonDuckApi::getByNameHeaderSortedSet);
        String result = subj.apply(new TreeSet<>(Arrays.asList("Launchpad", "Gasoline"))).get();
        Assertions.assertEquals("SortedSet: Gasoline,Launchpad", result);
    }

    // @Test
    // Matrix params not implemented yet. See #3 
    void getMatrixParam() throws Exception {
        Function1<String, CompletableFuture<String>> subj = resource.init1(JsonDuckApi::getByNameMatrix);

        String result = subj.apply("Launchpad").get();
        Assertions.assertEquals("matrix Launchpad", result);
    }
}