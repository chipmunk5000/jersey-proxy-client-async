package org.glassfish.jersey.client.async;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;

@SuppressWarnings("ResultOfMethodCallIgnored")
class MethodCaptorTest {

    @Test
    void captureGetString() {
        Method method = MethodCaptor.capture(A.class, A::getString);
        Assertions.assertEquals("getString", method.getName());
    }

    @Test
    void captureGetInt() {
        Method method = MethodCaptor.capture(A.class, A::getInt);
        Assertions.assertEquals("getInt", method.getName());
    }

    @Test
    void captureHashCode() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
            MethodCaptor.capture(A.class, Object::hashCode)
        );
    }

    @Test
    void captureEquals() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
            MethodCaptor.capture(A.class, a -> a.equals(null))
        );
    }

    @Test
    void captureToString() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
            MethodCaptor.capture(A.class, A::toString)
        );
    }

    @Test
    void captureGetStringAndGetInt() {
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () ->
            MethodCaptor.capture(A.class, a -> {
                a.getString();
                a.getInt();
            })
        );
        Assertions.assertTrue(e.getMessage().contains("getString"));
        Assertions.assertTrue(e.getMessage().contains("getInt"));
    }

    @Test
    void captureToStringAndGetValue() {
        Method method = MethodCaptor.capture(A.class, a -> {
            a.toString();
            a.getString();
        });
        Assertions.assertEquals("getString", method.getName());
    }

    @Test
    void captureGetValueAndToString() {
        Method method = MethodCaptor.capture(A.class, a -> {
            a.getString();
            a.toString();
        });
        Assertions.assertEquals("getString", method.getName());
    }

    @Test
    void captureToStringWithArgs() {
        Method method = MethodCaptor.capture(A.class, a -> a.toString("foo"));
        Assertions.assertEquals("toString", method.getName());
    }

    interface A {
        String getString();

        String toString(String s);

        int getInt();
    }
}