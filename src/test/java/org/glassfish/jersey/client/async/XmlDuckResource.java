package org.glassfish.jersey.client.async;

import javax.validation.Valid;

public class XmlDuckResource implements XmlDuckApi {
    @Override
    public XmlDuck postValid(@Valid XmlDuck entity) {
        return new XmlDuck(entity.name + " McDuck");
    }
}
